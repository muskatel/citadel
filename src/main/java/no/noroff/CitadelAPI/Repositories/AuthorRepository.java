package no.noroff.CitadelAPI.Repositories;

import no.noroff.CitadelAPI.Models.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Integer> {
    Author getByLastname(String lastname);
}
