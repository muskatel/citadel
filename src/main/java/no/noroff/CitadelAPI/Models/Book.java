package no.noroff.CitadelAPI.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Book {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String title;

    @JsonGetter("authors")
    public List<String> authors() {
        return authors.stream()
                .map(author -> {
                    return "/author/" + author.id;
                }).collect(Collectors.toList());
    }

    @ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(
            name = "Book_Author",
            joinColumns = { @JoinColumn(name = "book_id") },
            inverseJoinColumns = { @JoinColumn(name = "author_id") }
    )
    public Set<Author> authors = new HashSet<Author>();

    @Column
    public String isbn10;

    @Column
    public String isbn13;

    @Column
    public String published;

    @Column
    public String edition;

    @Column
    public String genre;

    @Column
    public HashSet<String> keywords;

}
