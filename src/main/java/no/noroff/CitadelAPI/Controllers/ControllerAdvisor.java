package no.noroff.CitadelAPI.Controllers;

import no.noroff.CitadelAPI.Models.CommonResponse;
import no.noroff.CitadelAPI.Models.ErrorResponse;
import no.noroff.CitadelAPI.Utils.Command;
import no.noroff.CitadelAPI.Utils.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<CommonResponse> handle(HttpServletRequest request, Exception ex) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.message = request.getRequestURI() + " was not found";
        //cr.error = new ErrorResponse("not_found", request.getRequestURI() + " was not found");
        //log and return
        cmd.setResult(HttpStatus.NOT_FOUND);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, HttpStatus.NOT_FOUND);
        }
}