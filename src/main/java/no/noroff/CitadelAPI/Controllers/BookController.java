package no.noroff.CitadelAPI.Controllers;

import no.noroff.CitadelAPI.Models.Author;
import no.noroff.CitadelAPI.Models.Book;
import no.noroff.CitadelAPI.Models.CommonResponse;
import no.noroff.CitadelAPI.Repositories.AuthorRepository;
import no.noroff.CitadelAPI.Repositories.BookRepository;
import no.noroff.CitadelAPI.Utils.Command;
import no.noroff.CitadelAPI.Utils.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/book")
    public ResponseEntity<CommonResponse> bookRoot(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        cr.message = "Not implemented";

        HttpStatus resp = HttpStatus.NOT_IMPLEMENTED;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/book/all")
    public ResponseEntity<CommonResponse> getAllBooks(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = bookRepository.findAll();
        cr.message = "All books";

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<CommonResponse> getBookById(HttpServletRequest request, @PathVariable Integer id){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(bookRepository.existsById(id)) {
            cr.data = bookRepository.findById(id);
            cr.message = "Book with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Book not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/book/search/{word}")
    public ResponseEntity<CommonResponse> getBookSearch(HttpServletRequest request, @PathVariable String word){
        Command cmd = new Command(request);


        String searchWord = word.toUpperCase();
        //process
        CommonResponse cr = new CommonResponse();
        List<Book> allBooks = bookRepository.findAll();
        ArrayList<Book> results = new ArrayList<Book>();

        for(Book book : allBooks){
            if(book.title != null && book.title.toUpperCase().contains(searchWord)) {
                results.add(book);
            }else if(book.keywords != null){
                for (String kw : book.keywords){
                    if(kw.toUpperCase().contains(searchWord)){
                        results.add(book);
                    }
                }
            }
        }

        cr.data = results;
        cr.message = "Results of book search for: " + word;

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/book")
    public ResponseEntity<CommonResponse> addBook(HttpServletRequest request, @RequestBody Book book){
        Command cmd = new Command(request);

        //process
        book = bookRepository.save(book);

        CommonResponse cr = new CommonResponse();
        cr.data = book;
        cr.message = "New book with id: " + book.id;

        HttpStatus resp = HttpStatus.CREATED;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PatchMapping("/book/{id}")
    public ResponseEntity<CommonResponse> updateBook(HttpServletRequest request, @RequestBody Book newBook, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(bookRepository.existsById(id)) {
            Optional<Book> bookRepo = bookRepository.findById(id);
            Book book = bookRepo.get();

            if(newBook.title != null) {
                book.title = newBook.title;
            }
            if(newBook.isbn10 != null) {
                book.isbn10 = newBook.isbn10;
            }
            if(newBook.isbn13 != null) {
                book.isbn13 = newBook.isbn13;
            }
            if(newBook.published != null) {
                book.published = newBook.published;
            }
            if(newBook.edition != null) {
                book.edition = newBook.edition;
            }
            if(newBook.genre != null) {
                book.genre = newBook.genre;
            }
            if(newBook.keywords != null) {
                book.keywords = newBook.keywords;
            }

            if(newBook.authors() != null && !newBook.authors().isEmpty()) {

                //TODO: remove all authors from book

                book.authors = new HashSet<Author>();

                for(Author author : newBook.authors) {
                    book.authors.add(author);

                   // authorRepository.findById(author.id).books;
                }
            }

            bookRepository.save(book);

            cr.data = book;
            cr.message = "Updated book with id: " + book.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Book not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<CommonResponse> replaceBook(HttpServletRequest request, @RequestBody Book newBook, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(bookRepository.existsById(id)) {
            Optional<Book> bookRepo = bookRepository.findById(id);
            Book book = bookRepo.get();

            book.title = newBook.title;
            book.isbn10 = newBook.isbn10;
            book.isbn13 = newBook.isbn13;
            book.published = newBook.published;
            book.edition = newBook.edition;
            book.genre = newBook.genre;
            book.keywords = newBook.keywords;
            book.authors = new HashSet<Author>();
            for(Author author : newBook.authors) {
                book.authors.add(author);
            }

            bookRepository.save(book);

            cr.data = book;
            cr.message = "Replaced book with id: " + book.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Book not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @DeleteMapping("/book/{id}")
    public ResponseEntity<CommonResponse> deleteBook(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
            cr.message = "Deleted book with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Book not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }
}
