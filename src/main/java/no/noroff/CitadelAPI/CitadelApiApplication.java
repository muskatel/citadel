package no.noroff.CitadelAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitadelApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(CitadelApiApplication.class, args);
	}
}
