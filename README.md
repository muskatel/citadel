# Citadel API - Java Edition

[![web](https://img.shields.io/static/v1?message=Offline&label=web&color=failure)](https://citadel-java.herokuapp.com/)

> Citadel API example originally for the Noroff Accelerate Course in Java

This project was originally used to demonstrate a complete API to students, it was also deployed to the then free tier of Heroku.
Since Heroku changed the pricing policy, I had to take down the live site.

As this project has not been used for three years, I am confident that the code will require some updating after checking dependencies for new versions.

## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

## License

Unlicensed
